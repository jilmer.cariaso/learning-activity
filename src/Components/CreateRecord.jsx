import React, { Component } from 'react';
import { CustomButton } from './CustomUI/CustomButton';
import { CustomTextField } from './CustomUI/CustomTextField';

/*
 * PROPS
 * - actionConfirm(data)
 * - actionClose
 */
export class CreateRecord extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            address: "",
            mobileNo: "",

            nameError: null,
            emailError: null,
            addressError: null,
            mobileNoError: null
        };
    }

    handleBasicValidation = (name, event) => {
        var isValid = event.target.value.trim().length > 0;
        this.setState({[name + "Error"]: (!isValid ? "Field is empty." : null)});
        this.handleOnChange(name, event);
    }

    handleEmailValidation = (name, event) => {
        var isValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(event.target.value);
        this.setState({[name + "Error"]: (!isValid ? "Invalid email format." : null)});
        this.handleOnChange(name, event);
    }

    handleNumberVaidation = (name, event) => {
        var isValid = event.target.value.length == 11;
        this.setState({[name + "Error"]: (!isValid ? "Must be 11 digit number." : null)});
        this.handleOnChange(name, event);
    }

    handleOnChange = (name, event) => {
        this.setState({[name]: event.target.value});
    }

    handleOnSubmit = () => {
        alert("Send");
    }

    renderTextField = (name, placeholder, value, error, validation, type) => {
        var style = {
            main: {
                margin: "1rem 0",
                position: "relative"
            },
            placeholder: {
                textAlign: "left",
                color: "#AAAAAA"
            },
            holder: {
                display: "inline-block",
                width: "20rem",
                fontSize: "0.75rem",
                padding: "0.5rem",
                border: "1px solid #AAAAAA",
                borderRadius: "0.25rem"
            },
            textfield: {
                width: "19rem",
                fontSize: "1rem",
                border: "none",
                outline: "none"
            },
            error: {
                fontSize: "0.75rem",
                display: "inline-block",
                padding: "0 1rem",
                color: "#FF0000"
            }
        };

        return (
            <div style={style.main}>
                <div style={style.holder}>
                    <div style={style.placeholder}>
                        {placeholder}
                    </div>
                    <input
                        style={style.textfield}
                        type={type == null ? "text" : type}
                        value={value} 
                        onChange={(e) => validation(name, e)} />
                </div>

                <div style={style.error}>
                    {error ? error : null}
                </div>

                <div style={{paddingBottom: "1.5rem"}} />
            </div>
        );
    }

    renderSubmit = () => {
        var style = {
            button: {
                width: "21rem",
                fontSize: "1rem",
                padding: "1rem 0.5rem",
                textAlign: "center",
                color: "#FFFFFF",
                background: "#555555",
                border: "none",
                outline: "none"
            },
            buttonDisabled: {
                width: "21rem",
                fontSize: "1rem",
                padding: "1rem 0.5rem",
                textAlign: "center",
                color: "#FFFFFF",
                background: "#999999",
                border: "none"
            }
        }

        var enabled = (
            this.state.name.trim().length > 0 && !this.state.nameError &&
            this.state.email.trim().length > 0 && !this.state.emailError &&
            this.state.address.trim().length > 0 && !this.state.addressError &&
            this.state.mobileNo.trim().length > 0 && !this.state.mobileNoError);

        return (
            <input 
                type="button"
                disabled={!enabled}
                style={enabled ? style.button : style.buttonDisabled}
                value="ADD USER"
                onClick={() => this.handleOnSubmit()} />
        );
    }

    render() {
        var canCreateRecord = (
            this.state.name.trim().length > 0 && !this.state.nameError &&
            this.state.email.trim().length > 0 && !this.state.emailError &&
            this.state.address.trim().length > 0 && !this.state.addressError &&
            this.state.mobileNo.trim().length > 0 && !this.state.mobileNoError);

        var style = {
            main: {
                position: "fixed",
                left: "0",
                top: "0",
                width: "100%",
                height: "100%",
                backgroundColor: "rgba(0, 0, 0, 0.5)",
                zIndex: "1"
            },
            mainWindow: {
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                backgroundColor: "#FFFFFF",
                boxShadow: "0 5px 5px #00000029",
                width: "25rem",
                padding: "5rem 3rem 3rem 3rem",
                textAlign: "center"
            }
        };
        return (
            <div style={style.main}>

                <div style={style.mainWindow}>

                    <CustomTextField 
                        placeholder="Name"
                        value={this.state.name} 
                        error={this.state.nameError}
                        validation={(e) => this.handleBasicValidation("name", e)} />

                    <CustomTextField 
                        placeholder="Email"
                        value={this.state.email} 
                        error={this.state.emailError}
                        validation={(e) => this.handleEmailValidation("email", e)} />

                    <CustomTextField 
                        placeholder="Address"
                        value={this.state.address} 
                        error={this.state.addressError}
                        validation={(e) => this.handleBasicValidation("address", e)} />

                    <CustomTextField 
                        placeholder="Mobile"
                        type="number"
                        value={this.state.mobileNo} 
                        error={this.state.mobileNoError}
                        validation={(e) => this.handleNumberVaidation("mobileNo", e)} />

                    {/*this.renderSubmit()*/}

                    <CustomButton 
                        text="ADD USER" 
                        normalColor="#009999" 
                        pointerColor="#00AAAA" 
                        margin="0 0.5rem"
                        disabled={!canCreateRecord}
                        action={() => this.props.actionConfirm(
                            {
                                id: Math.random().toString(36).substr(2, 9),
                                name: this.state.name,
                                email: this.state.email,
                                address: this.state.address,
                                mobileNo: this.state.mobileNo
                            }
                        )} />

                    <CustomButton 
                        text="CANCEL" 
                        normalColor="#FF4400" 
                        pointerColor="#FF6600" 
                        margin="0 0.5rem"
                        action={() => this.props.actionClose()} />
                </div>
            </div>
        );
    }
}