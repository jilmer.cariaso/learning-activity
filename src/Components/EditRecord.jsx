import React, { Component } from 'react';
import { CustomButton } from './CustomUI/CustomButton';
import { CustomTextField } from './CustomUI/CustomTextField';

/*
 * PROPS
 * - data
 * - actionConfirm(data)
 * - actionClose
 */
export class EditRecord extends Component {

    constructor(props) {
        super(props);

        // Must not use some thing like:
        // this.state.data = this.props.data
        // The values will be passed by reference if that's the case

        this.state = {
            id: this.props.data.id,
            name: this.props.data.name,
            email: this.props.data.email,
            address: this.props.data.address,
            mobileNo: this.props.data.mobileNo,

            nameError: null,
            emailError: null,
            addressError: null,
            mobileNoError: null
        };
    }

    handleBasicValidation = (name, event) => {
        var isValid = event.target.value.trim().length > 0;
        this.setState({[name + "Error"]: (!isValid ? "Field is empty." : null)});
        this.handleOnChange(name, event);
    }

    handleEmailValidation = (name, event) => {
        var isValid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(event.target.value);
        this.setState({[name + "Error"]: (!isValid ? "Invalid email format." : null)});
        this.handleOnChange(name, event);
    }

    handleNumberVaidation = (name, event) => {
        var isValid = event.target.value.length == 11;
        this.setState({[name + "Error"]: (!isValid ? "Must be 11 digit number." : null)});
        this.handleOnChange(name, event);
    }
    
    handleOnChange = (name, event) => {
        this.setState({[name]: event.target.value});
    }

    render() {
        var canUpdateRecord = (
            this.state.name.trim().length > 0 && !this.state.nameError &&
            this.state.email.trim().length > 0 && !this.state.emailError &&
            this.state.address.trim().length > 0 && !this.state.addressError &&
            this.state.mobileNo.trim().length > 0 && !this.state.mobileNoError);
            
        var style = {
            main: {
                position: "fixed",
                left: "0",
                top: "0",
                width: "100%",
                height: "100%",
                backgroundColor: "rgba(0, 0, 0, 0.5)",
                zIndex: "1"
            },
            mainWindow: {
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                backgroundColor: "#FFFFFF",
                boxShadow: "0 5px 5px #00000029",
                width: "25rem",
                padding: "5rem 3rem 3rem 3rem",
                textAlign: "center"
            }
        }

        return (
            <div style={style.main}>

                <div style={style.mainWindow}>

                    <CustomTextField 
                        placeholder="Name"
                        value={this.state.name} 
                        error={this.state.nameError}
                        validation={(e) => this.handleBasicValidation("name", e)} />

                    <CustomTextField 
                        placeholder="Email"
                        value={this.state.email} 
                        error={this.state.emailError}
                        validation={(e) => this.handleEmailValidation("email", e)} />

                    <CustomTextField 
                        placeholder="Address"
                        value={this.state.address} 
                        error={this.state.addressError}
                        validation={(e) => this.handleBasicValidation("address", e)} />

                    <CustomTextField 
                        placeholder="Mobile"
                        type="number"
                        value={this.state.mobileNo} 
                        error={this.state.mobileNoError}
                        validation={(e) => this.handleNumberVaidation("mobileNo", e)} />

                    <CustomButton 
                        text="CONFIRM" 
                        normalColor="#009999" 
                        pointerColor="#00AAAA" 
                        margin="0 0.5rem"
                        disabled={!canUpdateRecord}
                        action={() => this.props.actionConfirm(
                            {
                                id: this.state.id,
                                name: this.state.name,
                                email: this.state.email,
                                address: this.state.address,
                                mobileNo: this.state.mobileNo
                            }
                        )} />

                    <CustomButton 
                        text="CANCEL" 
                        normalColor="#FF4400" 
                        pointerColor="#FF6600" 
                        margin="0 0.5rem"
                        action={() => this.props.actionClose()} />
                </div>
                
            </div>
        );
    }
}