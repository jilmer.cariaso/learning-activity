import React, { Component } from 'react';
import { CustomButton } from './CustomUI/CustomButton';

/*
 * PROPS
 * - id
 * - actionConfirm(id)
 * - actionClose
 */
export class DeleteRecord extends Component {

    render() {
        var style = {
            main: {
                position: "fixed",
                left: "0",
                top: "0",
                width: "100%",
                height: "100%",
                backgroundColor: "rgba(0, 0, 0, 0.5)",
                zIndex: "1"
            },
            mainWindow: {
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                backgroundColor: "#FFFFFF",
                boxShadow: "0 5px 5px #00000029",
                width: "25rem",
                padding: "5rem 3rem 3rem 3rem",
                textAlign: "center"
            },
            header: {
                fontSize: "1.5rem",
                marginBottom: "1rem"
            }
        }
        
        return (
            <div style={style.main}>

                <div style={style.mainWindow}>

                    <div style={style.header}>
                        {"Delete record " + this.props.id + " ?"}
                    </div>

                    <CustomButton 
                        text="CONFIRM" 
                        normalColor="#009999" 
                        pointerColor="#00AAAA" 
                        margin="0 0.5rem"
                        action={() => this.props.actionConfirm(this.props.id)} />

                    <CustomButton 
                        text="CANCEL" 
                        normalColor="#FF4400" 
                        pointerColor="#FF6600" 
                        margin="0 0.5rem"
                        action={() => this.props.actionClose()} />
                </div>
            </div>
        );
    }
}