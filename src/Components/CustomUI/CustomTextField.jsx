import React, { Component } from 'react';

/*
 * PROPS
 * - placeholder
 * - type (can be text, number, password, etc, default: text)
 * - value
 * - error
 * - validation(e) (same to onChange)
 */
export class CustomTextField extends Component {

    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render() {

        var style = {
            placeholder: {
                textAlign: "left",
                color: "#AAAAAA"
            },
            holder: {
                fontSize: "0.75rem",
                padding: "0.5rem",
                border: "1px solid #AAAAAA",
                borderRadius: "0.25rem"
            },
            textfield: {
                width: "100%",
                fontSize: "1rem",
                border: "none",
                outline: "none"
            },
            error: {
                fontSize: "0.75rem",
                padding: "0 1rem",
                color: "#FF0000"
            }
        };

        return (
            <div>
                <div style={style.holder}>
                    <div style={style.placeholder}>
                        {this.props.placeholder}
                    </div>
                    <input
                        style={style.textfield}
                        type={this.props.type == null ? "text" : this.props.type}
                        value={this.props.value} 
                        onChange={(e) => this.props.validation(e)} />
                </div>

                <div style={style.error}>
                    {this.props.error ? this.props.error : null}
                </div>

                <div style={{paddingBottom: "1.5rem"}} />
            </div>
        );
    }
}