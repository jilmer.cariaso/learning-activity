import React, { Component } from 'react';

/*
 * PROPS
 * - text
 * - pointerColor
 * - normalColor
 * - disabled
 * - action 
 * - margin (same with margin in CSS)
 */
export class CustomButton extends Component {

    constructor(props) {
        super(props);

        this.state = {
            cursor: "none"
        };
    }

    render() {
        var background = 
            (this.props.disabled ? "#999999" : 
            (this.state.cursor === "pointer" ? this.props.pointerColor : 
            (this.props.normalColor)));

        var style = {
            button: {
                margin: this.props.margin,
                width: "7.5rem",
                fontSize: "1rem",
                padding: "0.75rem",
                textAlign: "center",
                color: "#FFFFFF",
                background: background,
                display: "inline-block",
                verticalAlign: "top",
                border: "none",
                borderRadius: "4px",
                outline: "none",
                cursor: this.state.cursor
            }
        };

        return (
            <input 
                style={style.button} 
                type="button" 
                disabled={this.props.disabled}
                value={this.props.text}
                onMouseEnter={() => this.setState({cursor: (this.props.disabled ? "not-allowed" : "pointer")})}
                onMouseLeave={() => this.setState({cursor: "none"})}
                onClick={() => this.props.action()} />
        );
    }
}