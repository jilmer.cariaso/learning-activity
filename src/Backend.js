const marklogic = require('marklogic');
var db = null;
export var qb = null;
var data = [];

// TESTING AREA START
// var connectionInfo = {
//     host: 'localhost',
//     port: '8000',
//     user: 'admin',
//     password: 'admin'
// }
// db = marklogic.createDatabaseClient(connectionInfo);
// qb = marklogic.queryBuilder;
// data = [
//     {id: 1, uid: Math.random().toString(36).substr(2, 9), name: "Jilmer John", address: "Phil.", email: "jjbc@gmail.com"},
//     {id: 2, uid: Math.random().toString(36).substr(2, 9), name: "Ludwig", address: "Germany", email: "ludwig@gmail.com"}
// ];

// db.documents.write(
//     data.map((item) => {
//         return {
//             uri: '/user/' + item.uid + '.json',
//             contentType: 'application/json',
//             collections: ['fake data'],
//             content: item
//         }
//     })
// )
// .result()
// .then(response => console.log(JSON.stringify(response)))
// .catch(error => console.error(error));

// TESTING AREA ENDS

export function init() {
    var connectionInfo = {
        host: 'localhost',
        port: '8000',
        user: 'admin',
        password: 'admin'
    }
    db = marklogic.createDatabaseClient(connectionInfo);
    qb = marklogic.queryBuilder;
}

export function save(data) {
    db.documents.write({
        uri: '/users.json',
        contentType: 'application/json',
        collections: ['users'],
        content: data
    })
    .result()
    .then(response => console.log("Data updated."))
    .catch(error => console.error(error));
}

export function getAll(callback) {
    db.documents.read('/users.json')
    .result()

    // If have already
    .then(documents => {
        var loadedData = documents[0].content;
        console.log(documents);
        callback(loadedData);
    })

    // If no data yet
    .catch(error => {
        console.log("No /users.json yet. Creating new one...");

        db.documents.write({
            uri: '/users.json',
            contentType: 'application/json',
            collections: ['users'],
            content: []
        })
        .result()
        .then(response => console.log("Blank /users.json file created."))
        .catch(error => console.error(error));
    });
}