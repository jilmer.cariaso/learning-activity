import React, { Component } from 'react';
import { CustomButton } from './Components/CustomUI/CustomButton';
import { EditRecord } from './Components/EditRecord';
import { DeleteRecord } from './Components/DeleteRecord';
import { CreateRecord } from './Components/CreateRecord';
import { getAll, save } from './Backend';

export class UsersTablePage extends Component {

    constructor(props) {
        super(props);

        this.state = { // Data must be from database
            createRecord: {
                disabled: true,
            },
            editRecord: {
                disabled: true,
                data: null
            },
            deleteRecord: {
                disabled: true,
                id: 0
            },
            data: {
                header: ["ID", "Name", "Email", "Address", "Mobile", "Actions"],
                content: [
                    {id: 1, name: "Jilmer Cariaso", email: "jjbcariaso@gmail.com", address: "Address", mobileNo: "09999999999"},
                    {id: 2, name: "Hernan Cortez", email: "hc@gmail.com", address: "Spain", mobileNo: "01111111111"},   
                ]
            }
        };

        this.updateData();
    }





    handleCreateButton = () => {
        var createRecord = {
            disabled: false
        };
        this.setState({createRecord: createRecord});
    } 
    
    handleEditButton = (id) => {
        var editRecord = {
            disabled: false,
            data: {}
        };

        for (var i = 0; i < this.state.data.content.length; i++) {
            if (id === this.state.data.content[i]['id']) {
                editRecord.data = this.state.data.content[i];
                break;
            }
        }
        this.setState({editRecord: editRecord});
    }

    handleDeleteButton = (id) => {
        var deleteRecord = {
            disabled: false,
            id: id
        };
        this.setState({deleteRecord: deleteRecord});
    }





    updateData = () => {
        getAll((loadedData) => {
            this.setState(prevState => ({
                data: {
                    ...prevState.data,
                    content: loadedData
                }
            }));
        });
    }




    
    saveCreateRecord = (data) => {
        // Show loading animation
        this.state.data.content.push(data);
        var joinedData = this.state.data.content;
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                content: joinedData
            }
        }));
        save(this.state.data.content);
        // Close loading animation 
        this.closeCreateRecordWindow();
    }
    
    closeCreateRecordWindow = () => {
        var createRecord = {
            disabled: true,
            id: 0
        }
        this.setState({createRecord: createRecord});
    }
    
    saveEditRecord = (data) => {
        // Show loading animation
        var content = this.state.data.content;
        for (var i = 0; i < content.length; i++) {
            if (content[i].id == data.id) {
                content[i] = data;
                break;
            }
        }
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                content: content
            }
        }));
        save(this.state.data.content);
        // Close loading animation
        this.closeEditRecordWindow();
    }

    closeEditRecordWindow = () => {
        var editRecord = {
            disabled: true,
            data: null
        };
        this.setState({editRecord: editRecord});
    }

    confirmDeleteRecord = (id) => {
        // Show loading animation
        var content = this.state.data.content;
        for (var i = 0; i < content.length; i++) {
            if (content[i].id == id) {
                content.splice(i, 1);
                break;
            }
        }
        this.setState(prevState => ({
            data: {
                ...prevState.data,
                content: content
            }
        }));
        save(this.state.data.content);
        // Close loading animation
        this.closeDeleteRecordWindow();
    }

    closeDeleteRecordWindow = () => {
        var deleteRecord = {
            disabled: true,
            id: 0
        }
        this.setState({deleteRecord: deleteRecord});
    }





    render() {

        var style = {
            main: {
                padding: "2rem 2rem"
            },
            table: {
                borderCollapse: "collapse"
            },
            header: {
                padding: "1rem 2rem",
                background: "#555555",
                color: "#FFFFFF"
            },
            cell: {
                padding: "1rem 2rem",
            }
        }

        return (
            <div style={style.main}>

                {!this.state.createRecord.disabled ? 
                    <CreateRecord 
                        actionClose={() => this.closeCreateRecordWindow()}
                        actionConfirm={(data) => this.saveCreateRecord(data)} /> 
                    : null}

                {!this.state.editRecord.disabled ? 
                    <EditRecord 
                        data={this.state.editRecord.data}
                        actionClose={() => this.closeEditRecordWindow()}
                        actionConfirm={(data) => this.saveEditRecord(data)} /> 
                    : null}

                {!this.state.deleteRecord.disabled ? 
                    <DeleteRecord 
                        id={this.state.deleteRecord.id}
                        actionClose={() => this.closeDeleteRecordWindow()}
                        actionConfirm={(id) => this.confirmDeleteRecord(id)} /> 
                    : null}

                <table style={style.table}>
                    <tr>
                        {this.state.data.header.map(val => <th style={style.header}>{val}</th>)}
                    </tr>
                    {this.state.data.content.map(record => (
                        <tr>
                            <td style={style.cell}>{record.id}</td>
                            <td style={style.cell}>{record.name}</td>
                            <td style={style.cell}>{record.email}</td>
                            <td style={style.cell}>{record.address}</td>
                            <td style={style.cell}>{record.mobileNo}</td>
                            <td style={style.cell}>
                                <CustomButton 
                                    text="EDIT" 
                                    normalColor="#009999" 
                                    pointerColor="#00AAAA"
                                    margin="0.5rem"
                                    action={() => this.handleEditButton(record.id)} />

                                <CustomButton 
                                    text="DELETE" 
                                    normalColor="#FF4400" 
                                    pointerColor="#FF6600"
                                    margin="0.5rem"
                                    action={() => this.handleDeleteButton(record.id)} />
                            </td>
                        </tr>
                    ))}
                </table>

                <CustomButton 
                    text="NEW USER" 
                    normalColor="#009999" 
                    pointerColor="#00AAAA"
                    margin="0.5rem"
                    action={() => this.handleCreateButton()} />
            </div>
        );
    }
}

