// Failed attempt, didn't work.

module.exports = {
    connInfo: {
        host: 'localhost',
        port: '8000',
        user: 'admin',
        password: 'admin'
    }
}

const marklogic = require('marklogic');
const myConnection = require('./my-connection-testing.js');
const db = marklogic.createDatabaseClient(myConnection.connInfo);

console.log(db);
const qb = marklogic.queryBuilder;

// This is just for testing
const documents = [
    {
        uri: '/gs/aardvark.json',
        content: {
            test: { id: 1 },
            name: 'aadrdvark',
            kind: 'mammal',
            desc: 'The aardvark is a medium-sized burrowing, nocturnal mammal.'
        }
    },

    {
        uri: '/gs/bluebird.json',
        content: {
            test: { id: 2 },
            name: 'bluebird',
            kind: 'bird',
            desc: 'The bluebird is a medium-sized, mostly insectivorous bird.'
        }
    },

    {
        uri: '/gs/cobra.json',
        content: {
            test: { id: 3 },
            name: 'cobra',
            kind: 'mammal',
            desc: 'The cobra is a venomous, hooded snake of the family Elapidae.'
        }
    },
];

// Use this function to write a document to database
db.documents.write(documents).result(
    function(response) {
        console.log("Success writing the document");
        console.log(response);
    },
    function(error) {
        console.log(error);
    }
);

// Use this function to read something from database
db.documents.query(
    qb.where(
        qb.byExample({test: {id: 1}})
    )
).result(
    function(documents) {
        console.log("Success reading the document");
        console.log(documents);
    },
    function(error) {
        console.log(error);
    }
)

// Use this function to read something from database