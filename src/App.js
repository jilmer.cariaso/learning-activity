import ReactDOM from 'react-dom';
import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';
import { UsersTablePage } from './UsersTablePage';
import { init, write } from './Backend';

export class App extends Component {

	constructor(props) {
		super(props);
		init();
	}
	
	render() {
		return (
			<Router>
				<Switch>
					<Route path="/" component={UsersTablePage} />
				</Switch>
			</Router>
		);
	}
}

ReactDOM.render(<App />, document.getElementById('root'));